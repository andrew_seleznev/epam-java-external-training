package com.epam.rentalpoint.domain.equipment;

import com.epam.rentalpoint.domain.Equipment;

/**
 * Equipment realisation
 */
public class RollerBlades extends Equipment {

	private static final long serialVersionUID = 1L;
	private int wheelsize;

    public int getWheelsize() {
        return wheelsize;
    }

    public void setWheelsize(int wheelsize) {
        this.wheelsize = wheelsize;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + wheelsize;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RollerBlades other = (RollerBlades) obj;
		if (wheelsize != other.wheelsize)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RollerBlades [wheelsize=");
		builder.append(wheelsize);
		builder.append("]");
		return builder.toString();
	}

}
