package com.epam.rentalpoint.domain.equipment;

import com.epam.rentalpoint.domain.Equipment;

/**
 * Equipment implementation
 */
public class Bicycle extends Equipment {

	private static final long serialVersionUID = 1L;
	private String type;

    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bicycle other = (Bicycle) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append("Bicycle [type=");
		builder.append(type);
		builder.append("]");
		return builder.toString();
	}
    
	

}
