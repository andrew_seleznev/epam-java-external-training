package com.epam.rentalpoint.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class RentStation {

	private Map<Equipment, Boolean> equipment;
	private Map<RentElement, Long> rentElements;
	private Map<Accessory, Long> accessories;

	public RentStation() {
		this.equipment = new HashMap<>();
		this.rentElements = new HashMap<>();
		this.accessories = new HashMap<>();
	}

	public Map<Equipment, Boolean> getEquipments() {
		return equipment;
	}

	public void setEquipments(Map<Equipment, Boolean> equipments) {
		this.equipment = equipments;
	}

	public Map<RentElement, Long> getRentElements() {
		return rentElements;
	}

	public void setRentElements(Map<RentElement, Long> rentElements) {
		this.rentElements = rentElements;
	}

	public Map<Accessory, Long> getAccessories() {
		return accessories;
	}

	public void setAccessories(Map<Accessory, Long> accessories) {
		this.accessories = accessories;
	}
	
	public void addEquipment(Equipment equipment) {
		this.equipment.put(equipment, true);
	}
	
	public void addRentElement(RentElement element) {
		this.rentElements.put(element, null);
	}

	public void excludeEquipment(Long id) {
		for (Entry<Equipment, Boolean> entry : equipment.entrySet()) {
			Long equipmentId = entry.getKey().getEquipmentId();
			if ((equipmentId != null) && (id == equipmentId)) {
				equipment.put(entry.getKey(), false);
			}
		}
	}
	
	public void excludeElement(Long elementId, Long personId) {
		for (Entry<RentElement, Long> entry : rentElements.entrySet()) {
			Long equipmentId = entry.getKey().getEquipment().getEquipmentId();
			if ((equipmentId != null) && (elementId == equipmentId)) {
				rentElements.put(entry.getKey(), personId);
			}
		}
	}
	
	public List<Equipment> findAvailableEquipments() {
		List<Equipment> list = new ArrayList<>();
		for (Entry<Equipment, Boolean> entry : equipment.entrySet()) {
			if (entry.getValue()) {
				list.add(entry.getKey());
			}
		}
		return list;
	}
	
	public List<RentElement> findAvailableElements() {
		List<RentElement> list = new ArrayList<>();
		for (Entry<RentElement, Long> entry : rentElements.entrySet()) {
			if (entry.getValue() == null) {
				list.add(entry.getKey());
			}
		}
		return list;
	}

}
