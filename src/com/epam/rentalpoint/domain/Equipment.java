package com.epam.rentalpoint.domain;

import java.io.Serializable;

/**
 * Domain class for all available equipment
 */
public abstract class Equipment implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
     * Unique equipment identifier
     */
    private Long equipmentId;
    /**
     * Equipment category
     */
    private EquipmentCategory category;
    /**
     * Equipment size if available
     */
    private String size;

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public EquipmentCategory getCategory() {
        return category;
    }
    public void setCategory(EquipmentCategory category) {
        this.category = category;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Equipment equipment = (Equipment) o;

        if (!equipmentId.equals(equipment.equipmentId)) return false;
        if (!category.equals(equipment.category)) return false;
        return size.equals(equipment.size);

    }

    @Override
    public int hashCode() {
        int result = equipmentId.hashCode();
        result = 31 * result + category.hashCode();
        result = 31 * result + size.hashCode();
        return result;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Equipment [equipmentId=");
		builder.append(equipmentId);
		builder.append(", category=");
		builder.append(category);
		builder.append(", size=");
		builder.append(size);
		builder.append("]");
		return builder.toString();
	}
    
}
