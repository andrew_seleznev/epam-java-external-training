package com.epam.rentalpoint.domain;


import java.io.Serializable;

/**
 * Domain class for all available accessories
 */
public abstract class Accessory implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long accessoryId;
    private String model;
    private AccessoryCategory category;

    public Long getAccessoryId() {
        return accessoryId;
    }
    public void setAccessoryId(Long accessoryId) {
        this.accessoryId = accessoryId;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public AccessoryCategory getCategory() {
        return category;
    }
    public void setCategory(AccessoryCategory category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Accessory accessory = (Accessory) o;

        if (!accessoryId.equals(accessory.accessoryId)) return false;
        if (!model.equals(accessory.model)) return false;
        return category.equals(accessory.category);

    }

    @Override
    public int hashCode() {
        int result = accessoryId.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + category.hashCode();
        return result;
    }
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Accessory [accessoryId=");
		builder.append(accessoryId);
		builder.append(", model=");
		builder.append(model);
		builder.append(", category=");
		builder.append(category);
		builder.append("]");
		return builder.toString();
	}

    
}
