package com.epam.rentalpoint.domain.accessories;

import com.epam.rentalpoint.domain.Accessory;


public class Kneecap extends Accessory {

	private static final long serialVersionUID = 1L;
	private Integer size;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return super.toString().concat("Kneecap{" +
                "size=" + size +
                '}');
    }
}
