package com.epam.rentalpoint.domain;

public enum EquipmentCategory {
	BIKE, ROLLER
}
