package com.epam.rentalpoint.domain;

public enum AccessoryCategory {
    HELMET, KNEECAP
}
