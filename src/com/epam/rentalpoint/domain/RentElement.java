package com.epam.rentalpoint.domain;

import java.util.Arrays;
import java.util.Set;

public class RentElement {
	
	private Equipment equipment;
	private Set<Accessory> accessories;
    private int [][] cost;
    
	public Equipment getEquipment() {
		return equipment;
	}
	
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}
	
	public Set<Accessory> getAccessories() {
		return accessories;
	}
	
	public void setAccessories(Set<Accessory> accessories) {
		this.accessories = accessories;
	}
	
	public void addAccessory(Accessory accessory){
        accessories.add(accessory);
    }
	
	public int[][] getCost() {
		return cost;
	}
	
	public void setCost(int[][] cost) {
		this.cost = cost;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accessories == null) ? 0 : accessories.hashCode());
		result = prime * result + Arrays.hashCode(cost);
		result = prime * result
				+ ((equipment == null) ? 0 : equipment.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RentElement other = (RentElement) obj;
		if (accessories == null) {
			if (other.accessories != null)
				return false;
		} else if (!accessories.equals(other.accessories))
			return false;
		if (!Arrays.deepEquals(cost, other.cost))
			return false;
		if (equipment == null) {
			if (other.equipment != null)
				return false;
		} else if (!equipment.equals(other.equipment))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("ID: ");
		builder.append(equipment.getEquipmentId());
		builder.append(" ");
		builder.append("Category: ");
		builder.append(equipment.getCategory());
		builder.append(" ");
		builder.append("Size: ");
		builder.append(equipment.getSize());
		builder.append(" ");
		builder.append("Cost: ");
		builder.append(Arrays.deepToString(cost));
		builder.append("\n");
		
		return builder.toString();
	}
    
}
