package com.epam.rentalpoint.main;

import com.epam.rentalpoint.domain.EquipmentCategory;
import com.epam.rentalpoint.domain.RentElement;
import com.epam.rentalpoint.domain.RentStation;
import com.epam.rentalpoint.domain.equipment.Bicycle;
import com.epam.rentalpoint.domain.equipment.RollerBlades;
import com.epam.rentalpoint.service.RentOperations;

/**
 * Created by Anton_Hubarevich on 10/7/2016.
 */
public class Runner {

	public static void main(String[] args) {
		RentStation rentStation = new RentStation();
		
		RentElement bikeElement = new RentElement();
		RentElement rollerBladesElement = new RentElement();
		
		Bicycle bicycle = new Bicycle();
		bicycle.setEquipmentId(1L);
		bicycle.setSize("M");
		bicycle.setType("road");
		bicycle.setCategory(EquipmentCategory.BIKE);
		
		RollerBlades rollerBlades = new RollerBlades();
		rollerBlades.setEquipmentId(2L);
		rollerBlades.setSize("38");
		rollerBlades.setWheelsize(86);
		rollerBlades.setCategory(EquipmentCategory.ROLLER);
		
		int[][] bicycleCost = new int[][] { { 1, 2, 3, 4 }, { 10, 8, 7, 5 } };
		int[][] rollerBladesCost = new int[][] { { 1, 2, 3, 4 }, { 12, 10, 8, 5 } };
		bikeElement.setEquipment(bicycle);
		bikeElement.setCost(bicycleCost);
		rollerBladesElement.setEquipment(rollerBlades);
		rollerBladesElement.setCost(rollerBladesCost);

		rentStation.addEquipment(bicycle);
		rentStation.addEquipment(rollerBlades);
		rentStation.addRentElement(bikeElement);
		rentStation.addRentElement(rollerBladesElement);
		
		RentOperations rentOperations = new RentOperations(rentStation);
		rentOperations.operate();

	}
}
