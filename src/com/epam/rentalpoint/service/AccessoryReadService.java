package com.epam.rentalpoint.service;

import com.epam.rentalpoint.domain.Accessory;
import com.epam.rentalpoint.domain.AccessoryCategory;
import com.epam.rentalpoint.domain.accessories.Helmet;
import com.epam.rentalpoint.domain.accessories.Kneecap;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class AccessoryReadService {


    public static List<Accessory> createAccessoryList(String path) {
        FileInputStream fstream = null;
        List<Accessory> accessories = new ArrayList<>();
        try {
            fstream = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                accessories.add(parseAccessory(strLine));
            }
            br.close();

        } catch (IOException e) {
            System.err.println("File not found");
            throw new RuntimeException(e);
        }
        return accessories;
    }

    private static Accessory parseAccessory(String line) {
        Accessory accessory = null;
        String[] data = line.split(",");
        String category = getValue(data[1]);
        switch (AccessoryCategory.valueOf(category.toUpperCase())) {
            case HELMET:
                accessory = buildHelmet(data);
                break;
            case KNEECAP:
                accessory = buildKneecap(data);
                break;
            default:
                System.err.println("No such accessory");
                break;
        }

        return accessory;
    }

    private static Accessory buildHelmet(String[] data) {
        Helmet helmet = new Helmet();
        helmet.setAccessoryId(Long.parseLong(getValue(data[0])));
        helmet.setCategory(AccessoryCategory.HELMET);
        helmet.setModel(getValue(data[2]));
        helmet.setSize(Integer.parseInt(getValue(data[3])));
        return helmet;
    }

    private static Accessory buildKneecap(String[] data) {
        Kneecap kneecap = new Kneecap();
        kneecap.setAccessoryId(Long.parseLong(getValue(data[0])));
        kneecap.setCategory(AccessoryCategory.KNEECAP);
        kneecap.setModel(getValue(data[2]));
        kneecap.setSize(Integer.parseInt(getValue(data[3])));
        return kneecap;
    }

    private static String getValue(String data) {
        return data.substring(data.indexOf(":") + 1, data.length());
    }

}
