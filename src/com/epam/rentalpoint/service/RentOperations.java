package com.epam.rentalpoint.service;

import java.util.List;
import java.util.Scanner;

import com.epam.rentalpoint.domain.Accessory;
import com.epam.rentalpoint.domain.Person;
import com.epam.rentalpoint.domain.RentElement;
import com.epam.rentalpoint.domain.RentStation;

public class RentOperations {

	private RentStation rentStation;
	private final static String ACCESSORIES_FILE_PATH = "resources/accessories.txt";
	private final static String LINE_DELIMITER = "----------";

	public RentOperations(RentStation rentStation) {
		this.rentStation = rentStation;
	}
	
	public RentStation getRentStation() {
		return rentStation;
	}

	public void operate() {
		List<Accessory> accessories = AccessoryReadService
				.createAccessoryList(ACCESSORIES_FILE_PATH);
		Scanner scanner = new Scanner(System.in);
		int operation = 0;
		do {
			System.out.println("To place order press 1");
			System.out.println("To show all accessories press 2");
			System.out.println("To show all available equipments press 3");
			System.out.println("To exit press 4");
			operation = scanner.nextInt();
			switch (operation) {
			case 1:
				bookRentElement();
				break;
			case 2:
				System.out.println(accessories);
				break;
			case 3:
				System.out.println("All available rent equipment");
				System.out.println(rentStation.findAvailableElements());
				break;
			case 4:
				System.out.println("Thank you!");
				scanner.close();
				break;
			default:
				System.out.println("Wrong choice");
				scanner.close();
				break;
			}
			System.out.println(LINE_DELIMITER);
		} while ((operation != 4));

	}
	
	private void bookRentElement() {
		Person person = new Person();
		Scanner scanner = new Scanner(System.in);
		Long elementId = 0L;
		do {
			System.out.println("Please, choose rent element ID or press 0 to exit");
			System.out.println("All rent elements: ");
			System.out.println(rentStation.findAvailableElements());
			elementId = scanner.nextLong();
			if (elementId != 0) {
				System.out.println("Please, type your first name: ");
				String par1 = scanner.next();
				person.setFirstName(par1);
				System.out.println("Please, type your last name: ");
				String par2 = scanner.next();
				person.setLastName(par2);
				System.out.println("Please, type your ID: ");
				person.setPersonId(scanner.nextLong());
				System.out.println("Please, type your phone number: ");
				person.setPhoneNumber(scanner.next());
				rentStation.excludeElement(elementId, person.getPersonId());
				System.out.println(LINE_DELIMITER);
				System.out.println("All available rent equipment");
				System.out.println(rentStation.findAvailableElements());
			}
		} while ((elementId != 0));
		scanner.close();
	}

}
